# UN PREP Font End

> Below, I described the entire presentation on the project environment, project structure, configuration files and description, etc.

## Getting started

- **First clone repository** :
```
   $ git clone git@gitlab.com:Mashpy/uniprep_laravel_design.git
```

- **Install all packages and dependencies** :
```
   $ npm install
```

- **Launch project**:
```
   $ gulp
```

## How Gulp Works!

  Gulp is a task-runner, at the beginning it needs to be installed and configured, brew or npm is used for installation!    
``` 
- install brew http://osxdaily.com/2018/03/07/how-install-homebrew-mac-os/
- install npm https://nodejs.org/en/  
```

After installing nam, you need to install gulp, everything is very simple in the console command:
```
- npm install gulp-cli -g
- npm install gulp -D
```


