//* Main Functions
getTimeRemaining = (endTime) => {
  let t = Date.parse(endTime) - Date.parse(new Date()),
    seconds = Math.floor((t / 1000) % 60),
    minutes = Math.floor((t / 1000 / 60) % 60),
    hours = Math.floor((t / (1000 * 60 * 60)) % 24),
    days = Math.floor(t / (1000 * 60 * 60 * 24));

  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

initTimer = (id, endTime) => {
  let clock = document.getElementById(id),
    daysSpan = clock.querySelector('.--days'),
    hoursSpan = clock.querySelector('.--hours'),
    minutesSpan = clock.querySelector('.--minutes'),
    secondsSpan = clock.querySelector('.--seconds'),
    timeInterval = setInterval(updateClock, 1000);

  function updateClock() {
    let gTime = getTimeRemaining(endTime);

    daysSpan.innerHTML = gTime.days;
    hoursSpan.innerHTML = ('0' + gTime.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + gTime.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + gTime.seconds).slice(-2);

    if (gTime.total <= 0) {
      clearInterval(timeInterval);
    }
  }

  updateClock();
}

starRating = (element, option, clickCallback, moveCallback) => {
  const width = $(element).width();

  if (option === undefined || option.hasOwnProperty("readyOnly")) {
    option = {
      readOnly: false,
      starCount: 5
    };
  }

  (option.hasOwnProperty("readyOnly") === false)
    ? $(element).addClass('gm-pointer')
    : $(element).removeClass('gm-pointer');

  $(element).each(function () {
    let _this = $(this);
    let defaultRatingValue = 0;
    let selectedRatingValue = 0;
    let dataCount = _this.data("count");
    console.log(dataCount);

    if (dataCount === undefined || dataCount === null || dataCount === "") {
      defaultRatingValue = (parseFloat(0) * 100) / option.starCount;
      selectedRatingValue = defaultRatingValue;
    } else {
      defaultRatingValue = (parseFloat(dataCount) * 100) / option.starCount;
      selectedRatingValue = defaultRatingValue;
    }

    changedPosition = ($this, position) => {
      $this.find(".rating__stars.--up").css("width", position + "%");
    }

    // Default rating value
    changedPosition(_this, defaultRatingValue);

    if (option.readOnly === false) {
      _this.on("mousemove", function (e) {
        let cX = getPosition(e);
        if (cX < 100) {
          changedPosition(_this, cX);

          if (moveCallback && typeof moveCallback !== undefined) {
            moveCallback(_this, round((cX / 100) * option.starCount));
          }
        }
      });

      _this.on("mouseleave", function (e) {
        changedPosition(_this, defaultRatingValue);

        if (moveCallback && typeof moveCallback !== undefined) {
          moveCallback(
            _this,
            round((defaultRatingValue / 100) * option.starCount)
          );
        }
      });

      _this.on("click", function (e) {
        selectedRatingValue = getPosition(e);
        defaultRatingValue = selectedRatingValue;
        changedPosition(_this, selectedRatingValue);

        if (clickCallback && typeof clickCallback !== undefined) {
          clickCallback(
            _this,
            round((selectedRatingValue / 100) * option.starCount)
          );
        }
      });
    }
  });

  round = (v) => {
    return Math.round(v * 100) / 100;
  }

  getPosition = (e) => {
    return (e.originalEvent.layerX / width) * 100;
  }
}
//* End Main Functions <<

//* Global Vars
const VIDEOS_SLIDER_CONFIG = {
  speed: 400,
  // autoplay: true,
  // centeredSlides: true,
  // direction: 'vertical',

  loop: true,
  loopedSlides: 100,
  // loopFillGroupWithBlank: true,

  // slidesPerGroup: 1,

  updateOnWindowResize: true,

  breakpoints: {
    320: {
      slidesPerView: 1,
      // spaceBetween: 24,
    },
    768: {
      slidesPerView: 1,
      spaceBetween: 32,
    },
    1024: {
      slidesPerView: 1,
      spaceBetween: 48,
    },
  },

  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
};

let deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
//* End Global Vars <<

//* Initialization
initTimer('bigSalesTimer', deadline);

starRating(
  "#starRating",
  {
    readOnly: false,
    starCount: 5
  }
);

toggleArrowButton = (elm) => {
  let parent = elm.parentElement;

  elm.classList.toggle('--is-toggled');

  parent.nextElementSibling.classList.toggle('--is-hide');
}

const tooltip$Elm = $('[data-toggle="tooltip"]');

const hideTooltip = () => {
  tooltip$Elm.tooltip('hide');
}
$(document).ready(function () {
  let tooltipCLoseBtn = '<button type="button" class="tooltip-btn fa fa-times-circle" onclick="hideTooltip()"></button>'

  tooltip$Elm.tooltip({
    boundary: 'window',
  })

  tooltip$Elm.on('inserted.bs.tooltip', function () {
    setTimeout(() => {
      $('.tooltip .tooltip-inner').append(tooltipCLoseBtn);
    }, 300)
  })

  $(window).on('scroll', function () {
    hideTooltip()
  });

  const swiper = new Swiper("#reviewsSlider", VIDEOS_SLIDER_CONFIG);

  const BANNER_ELEMENT = $('.course-info');
  const ORDER_CARD = $('.info-card');
  const HEADER_ELEMENT = $('.header');

  let stickyNavTop = HEADER_ELEMENT.offset().top;

  let stickyNav = function () {
    let scrollTop = $(window).scrollTop();

    if (scrollTop > 10) {
      BANNER_ELEMENT.addClass('--small')
      ORDER_CARD.addClass('--small')
      // $('body').addClass('--is-sticky-spacer')

    } else {
      BANNER_ELEMENT.removeClass('--small');
      ORDER_CARD.removeClass('--small');
      // $('body').removeClass('--is-sticky-spacer')
    }
  };

  if (BANNER_ELEMENT.length) {
    $(window).scroll(function () {
      stickyNav();
    });
  }


  const PLAN_CARD = $('.plan-card ');

  $("button").click(function () {
    $("p").hide(1000);
  });

  /*
  //var owl = $('.owl-carousel');
    $('#owl-demo').owlCarousel({
      margin: 10,
      loop: true,
      navigation: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1000: {
          items: 3
        }
      }
    });
  */

});
